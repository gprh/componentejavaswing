package com.gprhidro;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import javax.swing.BorderFactory;
import javax.swing.JButton;

public class JRButton extends JButton implements Cloneable {

    // Atributos e métodos 
    @Override
    public JRButton clone() throws CloneNotSupportedException {
        return (JRButton) super.clone();
    }

    //jRButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/novas/ic_add_circle_outline_black_18dp.png"))); // NOI18N
    
    public Color JR_Normal_StartColor = new Color(255, 255, 255);
    public Color JR_Normal_EndColor = Color.GREEN;
    public Color JR_Hover_StartColor = new Color(255, 0, 255);
    public Color JR_Hover_EndColor = Color.green;
    public Color JR_HoverColor = new Color(203, 235, 226);
    public Color JR_Hover_ForeGround = new Color(0, 153, 153);
    public Color JR_Normal_ForeGround ;
    public boolean JR_AllowGradient = false;
    public int JRBorderRadius = 10;
    private boolean mouseExited = false;
    private boolean mouseEntered = false;
    private boolean mousePressed = false;
    private boolean mouseReleased = false;
    public boolean JR_FillButton = true;    
    public Color JR_SelectedColor = new java.awt.Color(146, 220, 201);
    public Color JR_PressedColor_semIndicator = new java.awt.Color(255, 51, 51);
    public int JR_IndicatorLeft = 2;
    public int JR_IndicatorBottom = 0;
    public Color JR_IndicatorColor = new Color(0, 153, 153);
    public boolean JR_IndicatorAllow = true;
    public int IconTextGap_y = 0;               
    
    
    public JRButton() {     
        this.setBackground(Color.WHITE);
        this.setForeground(new Color(51, 51, 51));
        this.setPreferredSize(new Dimension(120, 45));        

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent me) {
                if (contains(me.getX(), me.getY())) {
                    mouseEntered = true;
                    repaint();
                }
            }

            @Override
            public void mouseExited(MouseEvent me) {
                mouseExited = true;
                mouseEntered = false;
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                mousePressed = false;
                mouseReleased = true;
            }

            @Override
            public void mousePressed(MouseEvent e) {
                mousePressed = true;
                mouseReleased = false;
            }

        };
        addMouseListener(mouseAdapter);
        
    }
    

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        super.setContentAreaFilled(false);

        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (mousePressed == true) {
            g2.setPaint(JR_PressedColor_semIndicator);
            if (isJR_IndicatorAllow()) {
                Component[] comp = getParent().getComponents();
                for (int i = 0; i < comp.length; i++) {
                    if (comp[i] instanceof JRButton) {

                        ((JRButton) comp[i]).setSelected(false);
                        ((JRButton) comp[i]).setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, JR_IndicatorColor));
                    }
                }
                this.setBorder(BorderFactory.createMatteBorder(0, JR_IndicatorLeft, JR_IndicatorBottom, 0, JR_IndicatorColor));

                this.setSelected(true);
            }

        } else {
            if (JR_AllowGradient == true) {
                GradientPaint gp = new GradientPaint(0, 0, JR_Normal_StartColor, 300, getHeight(), JR_Normal_EndColor);
                g2.setPaint(gp);                
                if (mouseEntered) {
                    gp = new GradientPaint(0, 0, JR_Hover_StartColor, 300, getHeight(), JR_Hover_EndColor);
                    g2.setPaint(gp);
                    setForeground(JR_Hover_ForeGround);
                } else if (mouseExited) {
                    gp = new GradientPaint(0, 0, JR_Normal_StartColor, 300, getHeight(), JR_Normal_EndColor);
                    g2.setPaint(gp);                    
                }

            } else {

                if (mouseEntered) {
                    g2.setPaint(JR_HoverColor);
                    g2.fillRoundRect(0, 0, getWidth(), getHeight(), JRBorderRadius, JRBorderRadius);
                    JR_Normal_ForeGround = new Color(this.getForeground().getRed(), this.getForeground().getGreen(), this.getForeground().getBlue());
                    setForeground(JR_Hover_ForeGround);
                } else if (mouseExited) {
                    g2.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, JRBorderRadius, JRBorderRadius);
                    g2.setPaint(this.getBackground());
                    
                    setForeground(JR_Normal_ForeGround);

                } else {
                    g2.setPaint(this.getBackground());
                }

            }
        }

        //trocarIconeNoHover();
        // g2.fillRect(0, 0, getWidth(), getHeight());
        if (isSelected()) {
            g2.setPaint(JR_SelectedColor);
            setForeground(this.getForeground());
        }
        if (JR_FillButton == true) {
            g2.fillRoundRect(0, 0, getWidth(), getHeight(), JRBorderRadius, JRBorderRadius);
        }
        g2.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, JRBorderRadius, JRBorderRadius);
        // The drawString(string) must be put after the setPaint(gradient)
        g2.setPaint(Color.BLACK);
        centerString(g, new Rectangle(getWidth(), getHeight()), getText(), getFont());

        // draw account;;; comentado.
//        try {
//            getIcon().paintIcon(this, g2, getHeight() / 2, 4);
//        } catch (Exception e) {
//        }
        drawIcons(g, new Rectangle(0, 0, getWidth(), getHeight()));
        
        //dispose
        g2.dispose();

    }

    public void centerString(Graphics g, Rectangle r, String s,
            Font font) {
        FontRenderContext frc
                = new FontRenderContext(null, true, true);

        Rectangle2D r2D = font.getStringBounds(s, frc);
        int rWidth = (int) Math.round(r2D.getWidth());
        int rHeight = (int) Math.round(r2D.getHeight());
        int rX = (int) Math.round(r2D.getX());
        int rY = (int) Math.round(r2D.getY());

        int a = (r.width / 2) - (rWidth / 2) - rX;
        int b = (r.height / 2) - (rHeight / 2) - rY;

        g.setFont(font);
        g.drawString(s, r.x + a, r.y + b);
    }

    public void drawIcons(Graphics g, Rectangle r) {

        try {
            FontRenderContext frc
                    = new FontRenderContext(null, true, true);

            Rectangle2D r2D = getFont().getStringBounds(getText(), frc);
            int rWidth = (int) Math.round(r2D.getWidth());
            int rHeight = (int) Math.round(r2D.getHeight());
            int rX = (int) Math.round(r2D.getX());
            int rY = (int) Math.round(r2D.getY());

            int a = (r.width / 2) - (rWidth / 2) - rX;
            int b = (r.height / 2) - (rHeight / 2) - rY;

            //Joserui alterou para ter controle da distancia em Y
            getIcon().paintIcon(this, g, getIconTextGap(), getIconTextGap_y());//(getHeight() / 3));

//            if (getIcon().getIconHeight() > getHeight() / 2) {
//                int zoomLevel = 10;
//                int newImageWidth = getIcon().getIconWidth() * zoomLevel;
//                int newImageHeight = getIcon().getIconHeight() * zoomLevel;
//                BufferedImage resizedImage = new BufferedImage(newImageWidth, newImageHeight, 0);
//                 resizedImage.createGraphics();
//                g.drawImage((Image) getIcon(), 0, 0, newImageWidth, newImageHeight, null);
//              
//            }
        } catch (Exception e) {
        }

    }

    /**
     * @return the JR_IndicatorBottom
     */
    public int getJR_IndicatorBottom() {
        return JR_IndicatorBottom;
    }

    /**
     * @param JR_IndicatorBottom the JR_IndicatorBottom to set
     */
    public void setJR_IndicatorBottom(int JR_IndicatorBottom) {
        this.JR_IndicatorBottom = JR_IndicatorBottom;
    }

    /**
     * @return the IconTextGap_y
     */
    public int getIconTextGap_y() {
        return IconTextGap_y;
    }

    /**
     * @param IconTextGap_y the IconTextGap_y to set
     */
    public void setIconTextGap_y(int IconTextGap_y) {
        this.IconTextGap_y = IconTextGap_y;
    }
    
    public boolean isJR_IndicatorAllow() {
        return JR_IndicatorAllow;
    }

    public void setJR_IndicatorAllow(boolean JR_IndicatorAllow) {
        this.JR_IndicatorAllow = JR_IndicatorAllow;
    }

    public int getJR_IndicatorLeft() {
        return JR_IndicatorLeft;
    }

    public void setJR_IndicatorLeft(int JR_IndicatorLeft) {
        this.JR_IndicatorLeft = JR_IndicatorLeft;

    }

    public Color getJR_IndicatorColor() {
        return JR_IndicatorColor;
    }

    public void setJR_IndicatorColor(Color JR_IndicatorColor) {
        this.JR_IndicatorColor = JR_IndicatorColor;
    }

    public Color getJR_PressedColor_semIndicator() {
        return JR_PressedColor_semIndicator;
    }

    public void setJR_PressedColor_semIndicator(Color JR_PressedColor_semIndicator) {
        this.JR_PressedColor_semIndicator = JR_PressedColor_semIndicator;
    }

    public Color getJR_SelectedColor() {
        return JR_SelectedColor;
    }

    public void setJR_SelectedColor(Color JR_SelectedColor) {
        this.JR_SelectedColor = JR_SelectedColor;
    }   

    public Color getJR_Normal_StartColor() {
        return JR_Normal_StartColor;
    }

    public void setJR_Normal_StartColor(Color JR_Normal_StartColor) {
        this.JR_Normal_StartColor = JR_Normal_StartColor;
    }

    public Color getJR_Normal_EndColor() {
        return JR_Normal_EndColor;
    }

    public void setJR_Normal_EndColor(Color JR_Normal_EndColor) {
        this.JR_Normal_EndColor = JR_Normal_EndColor;
    }

    public Color getJR_Hover_StartColor() {
        return JR_Hover_StartColor;
    }

    public void setJR_Hover_StartColor(Color JR_Hover_StartColor) {
        this.JR_Hover_StartColor = JR_Hover_StartColor;
    }

    public Color getJR_Hover_EndColor() {
        return JR_Hover_EndColor;
    }

    public void setJR_Hover_EndColor(Color JR_Hover_EndColor) {
        this.JR_Hover_EndColor = JR_Hover_EndColor;
    }

    public Color getJR_HoverColor() {
        return JR_HoverColor;
    }

    public void setJR_HoverColor(Color JR_HoverColor) {
        this.JR_HoverColor = JR_HoverColor;
    }

    public Color getJR_Hover_ForeGround() {
        return JR_Hover_ForeGround;
    }

    public void setJR_Hover_ForeGround(Color JR_Hover_ForeGround) {
        this.JR_Hover_ForeGround = JR_Hover_ForeGround;
    }

    public boolean isJR_AllowGradient() {
        return JR_AllowGradient;
    }

    public void setJR_AllowGradient(boolean JR_AllowGradient) {
        this.JR_AllowGradient = JR_AllowGradient;
    }

    public int getJRBorderRadius() {
        return JRBorderRadius;
    }

    public void setJRBorderRadius(int JRBorderRadius) {
        this.JRBorderRadius = JRBorderRadius;
    }

    public boolean isJR_FillButton() {
        return JR_FillButton;
    }

    public void setJR_FillButton(boolean JR_FillButton) {
        this.JR_FillButton = JR_FillButton;
    }

}
